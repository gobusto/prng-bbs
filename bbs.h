/**
@file
@brief A pseudo-RNG, based on http://en.wikipedia.org/wiki/Blum_blum_shub

Copyright (C) 2018 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __PRNG_BBS_H__
#define __PRNG_BBS_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long prng_int; /**< @brief Integer type used by the PRNG. */

/**
@brief A structure representing a random number generator.

This would usually be created via `bbsCreate()`
*/

typedef struct {
  prng_int m;     /**< @brief Result of (Prime Number A) * (Prime Number B) */
  prng_int seed;  /**< @brief The (current) seed value - never zero or one! */
} PRNG_BBS;

/**
@brief Create a new (malloc'd) Blum-Blum-Shub PRNG structure.

@param seed The initial seed value of the structure - should be at least 2.
@return A new structure on success, or NULL on failure.
*/

PRNG_BBS *bbsCreate(prng_int seed);

/**
@brief Free a previously-created Blum-Blum-Shub PRNG structure.

@param bbs The structure to be freed.
*/

void bbsDelete(PRNG_BBS *bbs);

/**
@brief Get the next number in the PRNG sequence.

@param bbs The PRNG to use.
@return A non-negative integer value.
*/

prng_int bbsGetValue(PRNG_BBS *bbs);

#ifdef __cplusplus
}
#endif

#endif /* __PRNG_BBS_H__ */
