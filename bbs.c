/**
@file
@brief A pseudo-RNG, based on http://en.wikipedia.org/wiki/Blum_blum_shub

Copyright (C) 2018 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "bbs.h"

/*
[PUBLIC] Create a new (malloc'd) Blum-Blum-Shub PRNG structure.
*/

PRNG_BBS *bbsCreate(prng_int seed) {
  PRNG_BBS *bbs = (PRNG_BBS*)malloc(sizeof(PRNG_BBS));

  if (!bbs)
    return NULL;

  bbs->m = 3559 * 3571;
  bbs->seed = seed < 2 ? 2 : seed;

  return bbs;
}

/*
[PUBLIC] Free a previously-created Blum-Blum-Shub PRNG structure.
*/

void bbsDelete(PRNG_BBS *bbs) {
  free(bbs);
}

/*
[PUBLIC] Get the next number in the PRNG sequence.
*/

prng_int bbsGetValue(PRNG_BBS *bbs) {
  if (!bbs || bbs->seed < 2 || bbs->m == 0)
    return 0;

  bbs->seed = (bbs->seed * bbs->seed) % bbs->m;
  return bbs->seed - 2;
}
