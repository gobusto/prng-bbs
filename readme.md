Blum-Blum-Shub Pseudorandom Number Generator
============================================

A basic implementation of Blum-Blum-Shub pseudorandom number generation in C.

The PRNG state is represented as a structure, so it's possible to have multiple
PRNG instances at once for different purposes, or perhaps for separate threads.

Example
-------

A basic program might look like this:

    #include <stdio.h>
    #include "bbs.h"

    int main(void) {
      int i;
      PRNG_BBS *prng = bbsCreate(1337);

      for (i = 0; i < 10; ++i)
        printf("%lu\n", bbsGetValue(prng));

      bbsDelete(prng);
      return 0;
    }

Running this code would produce the following result:

    1787567
    7794623
    2549635
    331779
    4346030
    4147270
    11200722
    748474
    7980643
    4662502

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
licence, allowing you to use or modify it for any purpose, including commercial
and closed-source projects. All I ask in return is that _proper attribution_ is
given (i.e. don't remove my name from the copyright text in the source code and
perhaps include me on the "credits" screen, if your program has such a thing).
